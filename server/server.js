const fs = require('fs');
const http = require('http');
// const https = require('https');
const path = require('path');

const express = require('express');
const proxy = require('express-http-proxy');
const expressStaticGzip = require('express-static-gzip');

// const privateKey = fs.readFileSync('privkey.pem', 'utf8');
// const certificate = fs.readFileSync('fullchain.pem', 'utf8');
// const credentials = { key: privateKey, cert: certificate };

const mainApp = express();

const PORT = process.env.PORT || '443';
const server = http.createServer(mainApp);
// if (PORT === '443') {
//   server = https.createServer(credentials, mainApp);
// }

const GATEWAY = 'http://localhost:5000';

// mainApp.use(express.static(path.resolve(__dirname, '..', 'build')));
mainApp.use(expressStaticGzip(path.join(__dirname, '..', 'dist')));
mainApp.set('trust proxy', process.env.TRUST_PROXY || 'loopback');
mainApp.use('/api', proxy(GATEWAY));

mainApp.get('/*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

server.listen(PORT, () => console.info(`UI Server running on port: ${PORT}`));
