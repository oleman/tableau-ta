import { expect } from 'chai';
import tableau from '@/store/tableau';

describe('Vuex: store/tableau.js', () => {
  it('$store.getters.filteredViews', () => {
    // mock state
    const state = {
      views: [
        {
          workbook: {
            tags: {},
            id: 'edc96a6e-2d2c-4723-b97b-54b6bd1c27bf',
            name: 'Cohort Analysis',
            description: 'Cohort analysis for Rupert demo',
            contentUrl: 'CohortAnalysis',
            showTabs: 'false',
            size: '1',
            createdAt: '2020-05-26T17:31:49Z',
            updatedAt: '2020-05-26T17:31:49Z',
            sheetCount: '1',
            hasExtracts: false,
          },
          owner: {
            email: 'yoni@hirupert.com',
            fullName: 'Yoni Steinmetz',
            id: '70a0ac4c-e259-4a41-8018-98e06e087d0f',
            lastLogin: '2020-06-03T03:49:03Z',
            name: 'yoni@hirupert.com',
            siteRole: 'SiteAdministratorCreator',
          },
          project: {
            id: 'b740d24f-c33e-4d0b-aa6a-5ef4f56573eb',
            name: 'default',
            description: 'The default project that was automatically created by Tableau.',
          },
          tags: {},
          usage: {
            totalViewCount: '4',
          },
          id: '650d86c4-7dcf-4683-8ced-243c2b50e98f',
          name: 'Cohort Analysis',
          contentUrl: 'CohortAnalysis/sheets/CohortAnalysis',
          createdAt: '2020-05-26T17:31:49Z',
          updatedAt: '2020-05-26T17:31:49Z',
          sheetType: 'dashboard',
          favoritesTotal: '0',
          viewUrlName: 'CohortAnalysis',
        },
        {
          workbook: {
            tags: {},
            id: '504d9fec-a9b1-4fd5-bc0f-44cf0bf073f7',
            name: 'Revenue Report',
            description: '',
            contentUrl: 'RevenueReport',
            showTabs: 'true',
            size: '1',
            createdAt: '2020-05-26T18:16:10Z',
            updatedAt: '2020-05-26T18:16:10Z',
            sheetCount: '2',
            hasExtracts: true,
          },
          owner: {
            email: 'adi@hirupert.com',
            fullName: 'Adi Cohen',
            id: '0e44a976-5e65-4e98-ba14-c5e64daa3ce0',
            lastLogin: '2020-06-04T12:49:22Z',
            name: 'adi@hirupert.com',
            siteRole: 'ExplorerCanPublish',
          },
          project: {
            id: 'b740d24f-c33e-4d0b-aa6a-5ef4f56573eb',
            name: 'default',
            description: 'The default project that was automatically created by Tableau.',
          },
          tags: {},
          usage: {
            totalViewCount: '13',
          },
          id: 'dee8d7e9-816c-440e-88af-c69d0f1f6c5f',
          name: 'Cohorts #Transactions by Country and Month (2016)',
          contentUrl: 'RevenueReport/sheets/CohortsTransactionsbyCountryandMonth2016',
          createdAt: '2020-05-26T18:16:10Z',
          updatedAt: '2020-05-26T18:16:10Z',
          sheetType: 'view',
          favoritesTotal: '0',
          viewUrlName: 'CohortsTransactionsbyCountryandMonth2016',
        },
        {
          workbook: {
            tags: {},
            id: 'd33b325a-9a9e-45d6-9f02-4448e323d593',
            name: '5-2-20 Follow ups dashboard',
            description: '',
            contentUrl: '5-2-20Followupsdashboard',
            showTabs: 'true',
            size: '1',
            createdAt: '2020-05-28T13:20:08Z',
            updatedAt: '2020-06-03T03:50:50Z',
            sheetCount: '3',
            hasExtracts: true,
          },
          owner: {
            email: 'adi@hirupert.com',
            fullName: 'Adi Cohen',
            id: '0e44a976-5e65-4e98-ba14-c5e64daa3ce0',
            lastLogin: '2020-06-04T12:49:22Z',
            name: 'adi@hirupert.com',
            siteRole: 'ExplorerCanPublish',
          },
          project: {
            id: 'b740d24f-c33e-4d0b-aa6a-5ef4f56573eb',
            name: 'default',
            description: 'The default project that was automatically created by Tableau.',
          },
          tags: {},
          usage: {
            totalViewCount: '1',
          },
          id: '1625a988-05aa-4f81-9957-ed2f678af983',
          name: 'Dashboard 1',
          contentUrl: '5-2-20Followupsdashboard/sheets/Dashboard1',
          createdAt: '2020-05-28T13:20:08Z',
          updatedAt: '2020-06-03T03:50:50Z',
          sheetType: 'dashboard',
          favoritesTotal: '0',
          viewUrlName: 'Dashboard1',
        },
      ],


      activeFilters: [
        ['workbook', '5-2-20 Follow ups dashboard'],
        ['owner', 'Adi Cohen'],
        ['created', '2020-05-28T13:20:08Z'],
      ],
      filterPaths: {
        workbook: 'workbook.name',
        owner: 'owner.fullName',
        created: 'createdAt',
      },
      sortRules: {
        workbook: 'workbook.name',
        owner: 'owner.fullName',
        name: 'name',
        created: 'createdAt',
      },
      sortRule: [],
    };

    // get the result from the getter
    const result = tableau.getters.filteredViews(state);

    // assert the result
    expect(result).to.deep.equal([
      {
        workbook: {
          tags: {},
          id: 'd33b325a-9a9e-45d6-9f02-4448e323d593',
          name: '5-2-20 Follow ups dashboard',
          description: '',
          contentUrl: '5-2-20Followupsdashboard',
          showTabs: 'true',
          size: '1',
          createdAt: '2020-05-28T13:20:08Z',
          updatedAt: '2020-06-03T03:50:50Z',
          sheetCount: '3',
          hasExtracts: true,
        },
        owner: {
          email: 'adi@hirupert.com',
          fullName: 'Adi Cohen',
          id: '0e44a976-5e65-4e98-ba14-c5e64daa3ce0',
          lastLogin: '2020-06-04T12:49:22Z',
          name: 'adi@hirupert.com',
          siteRole: 'ExplorerCanPublish',
        },
        project: {
          id: 'b740d24f-c33e-4d0b-aa6a-5ef4f56573eb',
          name: 'default',
          description: 'The default project that was automatically created by Tableau.',
        },
        tags: {},
        usage: {
          totalViewCount: '1',
        },
        id: '1625a988-05aa-4f81-9957-ed2f678af983',
        name: 'Dashboard 1',
        contentUrl: '5-2-20Followupsdashboard/sheets/Dashboard1',
        createdAt: '2020-05-28T13:20:08Z',
        updatedAt: '2020-06-03T03:50:50Z',
        sheetType: 'dashboard',
        favoritesTotal: '0',
        viewUrlName: 'Dashboard1',
      },
    ]);
  });
});
