// Libraries
// import Vue from 'vue';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

// Components
import NewTemplateDialog from '@/components/NewTemplateDialog.vue';

// Utilities
import {
  mount,
  createLocalVue,
} from '@vue/test-utils';

Vue.use(Vuetify);
const localVue = createLocalVue();
localVue.use(Vuex);

describe('NewTemplateDialog.vue', () => {
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  const mocks = {
    $store: {
      state: {
        tableau: {
          loading: false,
        },
      },
      dispatch() { },
    },
  };

  it('should a button "+ New Template"', () => {
    const wrapper = mount(NewTemplateDialog, {
      localVue,
      vuetify,
      mocks,
    });

    // With jest we can create snapshot files of the HTML output
    // expect(wrapper.html()).toMatchSnapshot();

    // We could also verify this differently
    // by checking the text content
    const button = wrapper.find('.v-btn__content');

    expect(button.text()).toBe('+ New Template');
  });

  it('should display the dialog window when the button is clicked', async () => {
    const wrapper = mount(NewTemplateDialog, {
      localVue,
      vuetify,
      mocks,
    });

    const button = wrapper.find('.new-template-btn');

    // Simulate a click on the button
    button.trigger('click');

    await wrapper.vm.$nextTick();

    const title = wrapper.find('.headline');

    expect(title.text()).toBe('Sign in to Tableau');
  });
});
