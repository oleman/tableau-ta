import axios from 'axios';
import API from './index';

const PORT = process.env.VUE_APP_TABLEAU_PORT === 'NULL' || !process.env.VUE_APP_TABLEAU_PORT ? '' : `:${process.env.VUE_APP_TABLEAU_PORT}`;
const API_ENDPOINT = `${process.env.VUE_APP_TABLEAU_HOST}${PORT}${process.env.VUE_APP_TABLEAU_PREFIX}`;

class TableauRESTDAO extends API {
  constructor() {
    super({
      baseUrl: API_ENDPOINT,
      client: axios.create({
        baseURL: API_ENDPOINT,
      }),
    });
    const parent = this.public;

    this.public = {
      signin: ({ ...options } = {}) => parent.post({ url: '/auth/signin', ...options }),

      /*
      * Query workbooks for site
      * https://help.tableau.com/current/api/rest_api/en-us/REST/rest_api_ref_workbooksviews.htm#query_workbooks_for_site
      *
      * Filtering and sorting
      * https://help.tableau.com/current/api/rest_api/en-us/REST/rest_api_concepts_filtering_and_sorting.htm
      */
      getViews: (siteId, { ...options } = {}) => parent.get({ url: `sites/${siteId}/views`, ...options }),

      getView: (siteId, viewId, { ...options } = {}) => parent.get({
        url: `sites/${siteId}/views/${viewId}`,
        ...options,
      }),

      getViewImage: (siteId, viewId, { ...options } = {}) => parent.get({
        url: `sites/${siteId}/views/${viewId}/image`,
        ...options,
      }),
    };
  }
}

const DAO = new TableauRESTDAO();
// const TableauREST = DAO.public;

export default DAO;
