import axios from 'axios';

export default class API {
  constructor(options = {}) {
    this.client = options.client
      || axios.create({
        baseURL: options.baseUrl,
      });
    this.baseURL = options.baseURL;

    this.client.interceptors.request.use(
      (r) => r,
      (error) => error,
    );

    this.client.interceptors.response.use(
      (response) => response,
      async (error) => (error.response ? error.response.data : error),
    );

    this.public = {
      get: ({ url = '', ...rest }) => this.client.get(url, rest),

      post: ({ url = '', ...rest }) => this.client.post(url, rest.data || rest, rest.config || {}),

      put: ({ url = '', ...rest }) => this.client.put(url, rest),

      patch: ({ url = '', ...rest }) => this.client.patch(url, rest),

      delete: ({ url = '', ...rest }) => this.client.delete(url, rest),
    };

    this.setAuthHeader = (token) => {
      this.client.defaults.headers.common['X-Tableau-Auth'] = token;
    };
  }
}
