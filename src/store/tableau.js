import _isEmpty from 'lodash/isEmpty';
import _filter from 'lodash/filter';
import _every from 'lodash/every';
import _get from 'lodash/get';
import _orderBy from 'lodash/orderBy';
import moment from 'moment';
import TableuAPI from '../api/tableau';

export default {
  state: () => ({
    credentials: null,
    error: null,
    loading: false,

    views: [],

    activeFilters: [
      ['workbook', null],
      ['owner', null],
      ['created', null],
    ],
    filterPaths: {
      workbook: 'workbook.name',
      owner: 'owner.fullName',
      created: 'createdAt',
    },
    sortRules: {
      workbook: 'workbook.name',
      owner: 'owner.fullName',
      name: 'name',
      created: 'createdAt',
    },
    sortRule: [],
  }),

  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },

    setCredentials(state, credentials) {
      state.credentials = credentials;
    },
    setViews(state, views) {
      state.views = views;
    },
    setFilter(state, filter) {
      state.activeFilters = state.activeFilters.map((item) => {
        if (item[0] === filter[0]) {
          return filter;
        }
        return item;
      });
    },
    setSortRule(state, rule) {
      state.sortRule = rule;
    },
  },

  actions: {
    async signin({ commit }, data) {
      commit('setError', null);
      commit('setLoading', true);

      try {
        const res = await TableuAPI.public.signin({
          data: {
            credentials: {
              name: data.name,
              password: data.password,
              site: {
                contentUrl: data.contentUrl,
              },
            },
          },
        });

        if (res.status !== 200) {
          throw new Error(res.error.detail);
        }

        TableuAPI.setAuthHeader(res.data.credentials.token);

        commit('setCredentials', res.data.credentials);
      } catch (err) {
        console.error(err);
        commit('setError', err.message || 'Network problem');
      }
      commit('setLoading', false);
    },

    async getViews({ state, commit }) {
      commit('setError', null);
      commit('setLoading', true);
      let views = [];

      try {
        if (_isEmpty(state.credentials)) return views;

        const res = await TableuAPI.public.getViews(state.credentials.site.id, {
          params: {
            fields: '_all_',
          },
        });

        if (res.status !== 200) {
          throw new Error(res.error.detail);
        }

        views = res.data.views.view;
        commit('setViews', views);
      } catch (err) {
        console.error(err);
        commit('setError', err.message || 'Network problem');
      }
      commit('setLoading', false);
      return views;
    },

    async getViewImage({ state, commit }, viewId) {
      commit('setError', null);
      let image = null;

      try {
        if (_isEmpty(state.credentials)) return image;

        const res = await TableuAPI.public.getViewImage(state.credentials.site.id, viewId, {
          responseType: 'arraybuffer',
        });

        if (res.status !== 200) {
          throw new Error(res.error && res.error.detail);
        }

        const base64 = btoa(
          new Uint8Array(res.data).reduce(
            (data, byte) => data + String.fromCharCode(byte),
            '',
          ),
        );
        image = `data:;base64,${base64}`;
      } catch (err) {
        console.error(err);
        commit('setError', err.message || 'Network problem');
      }
      return image;
    },
  },

  getters: {
    filteredViews: (state) => {
      const filters = state.activeFilters.filter((filter) => filter[1]);

      const filteredViews = _filter(state.views, (view) => _every(filters,
        ([path, value]) => {
          if (path === 'created') {
            return moment(value).isSame(
              moment(_get(view, state.filterPaths[path])),
              'day',
            );
          }
          return _get(view, state.filterPaths[path]) === value;
        }));

      return _orderBy(
        filteredViews,
        [state.sortRules[state.sortRule[0]]],
        [state.sortRule[1]],
      );
    },
    allWorkbooks: (state) => {
      const workbooks = [];

      state.views.forEach((view) => {
        const workbook = workbooks.find((item) => item === view.workbook.name);
        if (!workbook) workbooks.push(view.workbook.name);
      });
      return workbooks;
    },
    allOwners: (state) => {
      const owners = [];

      state.views.forEach((view) => {
        const owner = owners.find((item) => item === view.owner.fullName);
        if (!owner) owners.push(view.owner.fullName);
      });
      return owners;
    },
  },
};
